[![pipeline status](https://gitlab.com/danwin/fairways_py/badges/master/pipeline.svg)](https://gitlab.com/danwin/fairways_py/commits/master)

[![Documentation Status](https://readthedocs.org/projects/fairways-py/badge/?version=latest)](https://fairways-py.readthedocs.io/en/latest/?badge=latest)

## Installation ##
```$ pip install fairways```
## Documents ##
https://fairways-py.readthedocs.io/en/latest/

## Warning ##
Some modules are proof-of-concept. Avoid to use this package in production without comprehensive test suite for your app.

## License ##

Licensed under either of the following, at your option:

Apache License, Version 2.0 (LICENSE-APACHE or
https://www.apache.org/licenses/LICENSE-2.0)
MIT License (LICENSE-MIT or
https://opensource.org/licenses/MIT)