Module io.syn.base
======================

Syncronous base classes.

Module content
--------------

.. automodule:: fairways.io.syn.base
   :members:
