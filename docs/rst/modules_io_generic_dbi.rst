Module io.generic.dbi
======================

Generic entities specific to SQL/CQL database.


.. automodule:: fairways.io.generic.dbi
   :members:
