Entities
========

Base classes to derive decorators which keeps data about wrapped function and associated metadata per each invocation.


.. automodule:: fairways.decorators.entities
   :members:
