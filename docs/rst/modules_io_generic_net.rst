Module io.generic.net
======================

Generic entities for HTTP, AMQP, Redis.


.. automodule:: fairways.io.generic.net
   :members:
