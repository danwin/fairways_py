Entrypoint
==========

Decorators to mark functions as handlers for different type of invocations or requests.

.. automodule:: fairways.decorators.entrypoint
   :members:
