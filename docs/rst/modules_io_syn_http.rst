Module io.syn.http
======================

Syncronous HTTP driver.
This implementation targeted RESTful services with json response.
Supports Basic Auth.

.. automodule:: fairways.io.syn.http
   :members:



