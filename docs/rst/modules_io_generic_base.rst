Module io.generic.base
======================

Generic entities to arrange interaction with IO.


.. automodule:: fairways.io.generic.base
   :members:
