Module io.asyn.base
======================

Asyncronous base classes.

Module content
--------------

.. automodule:: fairways.io.asyn.base
   :members:
