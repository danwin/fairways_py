.. Fairways.Py documentation master file, created by
   sphinx-quickstart on Wed Jan 15 17:46:44 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Fairways.Py's documentation!
=======================================

Contents:

.. toctree::
   :maxdepth: 2
   
   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

